/*!
 * jQuery Nearby Plugin v0.9.0 (http://terrason.oschina.io/jquery-nearby/)
 * Copyright 2017 jterraghost@gmail.com
 * Licensed under the MIT license
 */
!function ($) {
    var CONTEXT = [".form-group", "form", ".dialog", document];

    /**
     * Find nearby element indicated by selector with searching context priority : `.form-group -> form -> .dialog`.
     * @param selector the finding target. It can be a integer represent siblings index, or any css selector, or the name of target element.
     * @param limitContext A option context limit searching outbound. Default is document.
     * @returns {jQuery} the elements found. May be Empty if not found.
     */
    $.fn.nearby = function (selector, limitContext) {
        if (Number.isInteger(selector)) {
            return this.siblings(":eq(" + selector + ")");
        }
        if (!selector) {
            return $();
        }
        if (/^#/.test(selector)) {
            return $(selector);
        }
        if (/[.[>~:]/.test(selector)) {
            return findNearby(this, selector, limitContext);
        }
        if (/^(body|form|select|input|textarea|div|span|h\d|table|tr|td|th|p|header|section|fieldset)$/.test(selector)) {
            return findNearby(this, selector, limitContext);
        }
        return findNearby(this, "[name=" + replaceMetacharator(selector) + "]", limitContext);
    };

    Number.isInteger = Number.isInteger || function (value) {
        return typeof value === 'number' &&
            isFinite(value) &&
            Math.floor(value) === value;
    };

    function findNearby($el, selector, limitContext) {
        var stack = limitContext;
        if (!stack) stack = CONTEXT;
        if (!$.isArray(stack)) stack = [stack];

        for (var i = 0; i < stack.length; i++) {
            var c = stack[i] === document ? $(document) : $el.closest(stack[i]);
            if (c.length) {
                var result = c.find(selector);
                if (result.length) {
                    return result;
                }
            }
        }
        return $();
    }

    function replaceMetacharator(metastring) {
        if (metastring && metastring.length)
            return metastring.replace(/\./, "\\.");
        else {
            return "";
        }
    }
}(jQuery);