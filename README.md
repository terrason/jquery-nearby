Which HTML Attribute do I hate the most? That is `id`. I wrote this to avoid querying element by id.

This is a jQuery plugin to find the element matching a selector that is somewhere nearby :-)

Image a scenario of User Mobile Binding:
```html
<form class="form-horizontal">
    <div class="form-group">
        <label class="col-xs-2 control-label" for="binding-mobile">
            手机号码：
        </label>
        <div class="col-xs-9">
            <input class="form-control" id="binding-mobile" type="tel" placeholder="手机号码"
                   name="mobile"/>
            <input class="form-control" id="binding-smscode" placeholder="验证码"
                   name="smscode"/>
            <button class="btn btn-link" type="button" id="js-smscode">
                <span class="state normal">获取验证码</span>
                <span class="state alter"><span class="code" data-bind="cooldown">0</span>秒后可重新获取</span>
            </button>
        </div>
    </div>
</form>
```
Use Jquery Nearby to help you:
```javascript
var $element=$("#js-smscode");//the base point of searching

$element.nearby(1);  //equals to `.siblings(":eq(1)")`
$element.nearby("#binding-mobile");
$element.nearby('input[name="mobile"]');
$element.nearby("mobile");  // equals to `.nearby("[name=mobile]")`
```

The above all found `#binding-mobile` element. Especially the `.nearby(name)` form works gracefully,
Simple string parameter that test failed with `/^#/` or `/[.[>~:]/` is considered to name, EXCEPT:
```
body|form|select|input|textarea|div|span|h\d|table|tr|td|th|p|header|section|fieldset
```
Jquery Nearby find out the target through these steps:

1. If parameter is int then just call `.siblings(":eq()")` instead.
2. Else if parameter is start with `#` then find global document. (Contains same id in one page is your fault :<)
3. Else find from closest `.form-group`.
4. If find nothing, go more far, and find from closest `form` context.
5. If find nothing, continue findind from closest `.dialog` context.
6. Finally, find from global `document`. You can specify a searching order by the second parameter. 
```
$element.nearby("mobile", [".form","#myDialog"]);
```

So at last Jquery Nearby WON'T return `undefined|null|false` , it returns EMPTY JQUERY object if found nothing.